package com.innominds.flipazon;

import com.innominds.flipazon.entities.Product;
import com.innominds.flipazon.entities.Order;
import com.innominds.flipazon.entities.Customer;
import com.innominds.flipazon.entities.Payment;
import com.innominds.flipazon.entities.dependencies.Address;
import com.innominds.flipazon.entities.dependencies.Phnno;
import com.innominds.flipazon.entities.dependencies.Rating;

/**
 * This is an implementation of a Main class Tester in which calling back the
 * constructors and instance instance variables from phnno,address,rating.
 * @author ziya
 */



public class Tester {
	public static void main(String[] args) {
		
		//creating an objects to insert new products
		
		Product myFirstproduct = new Product("Smart watch", "excellent", "pink", 2000f);
		System.out.println(myFirstproduct);
		Product mySecondproduct = new Product("leather watch", "poor Quality", "brown", 1000f);
		System.out.println(mySecondproduct);
		Product myThirdproduct = new Product("shoe", "good one", "white", 2500f);
		System.out.println(myThirdproduct);
		System.out.println("\n");
		
		//Created a customer class object to call the customer class variables
	    // in the main method
		
		System.out.println("\n");
		System.out.println("******************");
		Customer myFirstcustomer = new Customer("101", "ziya", "shaik", "a1@gmail.com", null, null);
		System.out.println(myFirstcustomer);
		Customer mySecondcustomer = new Customer("102", "tajmun", "syed", "b1@gmail.com", null, null);
		System.out.println(mySecondcustomer);
		Customer myThirdcustomer = new Customer("103", "tasleem", "mohammad", "c1@gmail.com", null, null);
		System.out.println(myThirdcustomer);
		System.out.println("\n");
		
		//Created Address class object and assigning values to the address class variables.
	     
	     System.out.println("******************");
		 System.out.println("Home Address of the customer \n");
		 Address myFirstAdd = new Address("16-2-34","imam","pinpadu","tenali","gnt","Ap","india","522202");//Home address
		 System.out.println(myFirstAdd);
		 
		 System.out.println("Work Address of the customer \n");
		 Address mySecondAdd = new Address("13-8-12","khan","nazarpet","chirala","prakasam","AP","india","533303");//Work address
		 System.out.println(mySecondAdd);
		 
		//created Order class object and calling a product 
		 
		 System.out.println("******************");
		 System.out.println("Customer odered product is : \n");
		 Order myOrder = new Order(myFirstproduct);
		 
		 System.out.println("******************");
		 System.out.println("payment details of the product : \n");
		 Payment mypayment = new Payment(myFirstproduct);
		 
		 System.out.println("******************");
		 System.out.println("customer contact details: \n ");
		
		 Phnno mycontactdetails = new Phnno("+91", "9703545427");
		 System.out.println(mycontactdetails);		
		 
		 
		 
		 
		
		
	}
	
}
