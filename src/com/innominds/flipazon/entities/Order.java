package com.innominds.flipazon.entities;


import java.util.Date;

/**
 * This is the implementation of order class which gives details about order.
 * @author ziya
 */
public class Order {	
	private static int order_id_counter = 134;
	
	String orderID;				// ID of the order	
	Product orderProduct;		// list of products belonging to the order
	Date dateOfOrder;			// date on which delivery will happen	
	/**
	 * created a order class constructor and calling the product class in the arguments 
	 */
	public Order(Product productToOrder)
	{
		this.orderID = ++order_id_counter + "";
		this.orderProduct = productToOrder;
		this.dateOfOrder = new Date();
		
		System.out.println("order id is                    :"+this.orderID);
		System.out.println("Date of order of the product   :"+this.dateOfOrder.toString());
		System.out.println(productToOrder); 
	}
}