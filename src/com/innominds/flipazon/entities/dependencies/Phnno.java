package com.innominds.flipazon.entities.dependencies;
/**
 * creating a class named contact details to give the contact details
 * @author ziya
 */
public class Phnno {
	enum phn_no {PRIMARY_NUMBER,ALTERNATE_NUMBER};
		
	    String countrycode;
	    String mobilenum; 
	    /**
	     * Created a constructor with arguments.
	     * @param countrycode
	     * @param mobilenum
	     */
		public Phnno(String countrycode,String mobilenum)
		{
			this.countrycode = countrycode;
			this.mobilenum = mobilenum;		
		}
		@Override
			public String toString() {
			String StringToReturn = "";
			StringToReturn += "countrycode id     :"+this.countrycode +    "\n";
			StringToReturn += "mobile number is   :"+this.mobilenum +      "\n";
			return StringToReturn;
			}
	}

