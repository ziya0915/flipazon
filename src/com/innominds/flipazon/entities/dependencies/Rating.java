package com.innominds.flipazon.entities.dependencies;

/**
 * This is the implementation of the "Rating" class
 * This class belongs to the rating given by
 *  each of the individual customer
 *   against product
 * @author ziya
 */

public class Rating {
	private String customer_id;  //this is the customer_id which is inherit by customer class
	private byte rating_number;  //this is the rating from(1-5)given by the customer
	private String rating_text;  //this is the review that is given by the customer
	
	public Rating(String customer_id,byte rating_number,String rating_text) {
		System.out.println("Rating constructor called");
		this.customer_id  = customer_id;
		this.rating_number = rating_number;
		this.rating_text   = rating_text;
		System.out.println("Ratings are below :");
		System.out.println("customer_id         :"+customer_id);
		System.out.println("rating_number      :"+rating_number);
		System.out.println("rating_text      :"+rating_text);
		 
	}
}
