package com.innominds.flipazon.entities.dependencies;
/**
 * creating a class Address to get the customer Address details
 * @author ziya
 */

 public class Address {
      enum title{HOME,OFFICE,OTHERS};
      String h_no;
      String street;
      String landmark;
      String city;
      String district;
      String state;
      String country;
      String pincode;
  	/**
  	 * created a constructor with instance arguments
  	 * @param h_no
  	 * @param street
  	 * @param landmark
  	 * @param city
  	 * @param district
  	 * @param state
  	 * @param country
  	 * @param pincode
  	 */
      public Address(String h_no, String street, String landmark, String city, String district, String state,String country,String pincode) 
  	{
  		this.h_no = h_no;
  		this.street = street;
  		this.landmark = landmark;
  		this.city = city;
  		this.district = district;
  		this.state = state;
  		this.country = country;
  		this.pincode = pincode;
  	}
  	 @Override
  	public String toString() 
  	   {
  		String StringToReturn = "";
  		StringToReturn += "h_no       :     "+this.h_no+               "\n";
  		StringToReturn += "street     :     "+this.street+             "\n";
  		StringToReturn += "landmark   :     "+this.landmark+           "\n";
  		StringToReturn += "city       :     "+this.city+               "\n";
  		StringToReturn += "district   :     "+this.district+           "\n";
  		StringToReturn += "State      :     "+this.state+              "\n";
  		StringToReturn += "country    :     "+this.country+            "\n";
  		StringToReturn += "Pincode    :     "+this.pincode+            "\n";
  		return StringToReturn;
  		}
      
}
