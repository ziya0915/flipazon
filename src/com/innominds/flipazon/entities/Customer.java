package com.innominds.flipazon.entities;
import java.util.ArrayList;
import com.innominds.flipazon.entities.dependencies.Address;
import com.innominds.flipazon.entities.dependencies.Phnno;

/** 
 * creating a parent class customer to get all the customer details
 * @author ziya
 */
public class Customer {
	String cust_id;
	String first_name;
	String last_name;
	String email;
	enum gender{FEMALE,MALE,OTHERS};
	ArrayList<Address>     address;
	ArrayList<Phnno>        phn_no;
	public Customer(String cust_id,String first_Name,String last_Name,String email, String last_name, String first_name)
	{
		this.cust_id = cust_id;
		this.first_name = first_name;
		this.last_name  = last_name;
		this.email      = email;
	     System.out.println("customer Details  \n");
		 System.out.println("cust_id         :"+cust_id);
		 System.out.println("first_name      :"+first_name);
		 System.out.println("last_name       :"+last_name);
		 System.out.println("email           :"+email);
		 
	}
	
	

}

