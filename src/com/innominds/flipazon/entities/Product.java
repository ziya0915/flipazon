package com.innominds.flipazon.entities;

/**
 * This is an implementation of class Product and initializing variables and providing the product details.   
 * @author ziya
 */

public class Product {
	private static int id_counter = 1001;
	public String id;
	String name;
	String description;
	String color;
	float price;
	
	//basic constructor
	/**
	 * A constructor with arguments calling the instance variables
	 * @param name
	 * @param description
	 * @param color
	 * @param price
	 */
	
	public Product(String name,String description,String color,float price){
		this.id = String.valueOf(id_counter++);
		this.name          = name;
		this.description   = description;
		this.color         = color;
		this.price         = price;
   }
	@Override
	public String toString() {
		String StringToReturn = "Product details are below : "	+"\n";
		StringToReturn +="id :           "+this.id          +  "\n";
		StringToReturn +="name :         "+this.name        +  "\n";
		StringToReturn +="description :  "+this.description +  "\n";
		StringToReturn +="color :        "+this.color       +  "\n";
		StringToReturn +="price :        "+this.price       +  "\n";
		return StringToReturn;
	}

}
